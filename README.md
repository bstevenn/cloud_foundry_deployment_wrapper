#Overview
This repo currently contains two wrapper scripts around Bosh Boot Loader (bbl),
bosh-init, bosh-cli, terraform,  bosh-deployment, and cf-deployment.

One script is for deploying a full Cloud Foundry stack in AWS
The other script is for deploying Cloud Fondry with Bosh-light on Virtualbox on a Mac

The deployment process seems to be changing rapidly so the validity of this 
will likely be short lived if not regularly updated.

###Key Files
```bash
├── bosh-cf-deployer.sh
├── bosh-cf-setup-vars-template.txt
├── bosh-lite-cf-deployer-mac-vbox.sh
├── deployment-vars.yml
├── operations
│   ├── aws-2-az-region.yml
│   ├── aws-rds-mysql.yml
│   ├── _aws-s3-blobstore-iam-policy.json
│   └── aws-s3-blobstore.yml
└── README.md
```


`bosh-cf-deployer.sh`
This script will source a variable file (bosh-cf-setup-vars-template.txt)
and install pre-reqs, clone repos, deploy a cloudformation template, 
deploy bosh and NAT server, deploy 2 ELBs, deploy cloudfoundry, and return 
the creds to login to cloudfoundry.

`bosh-lite-cf-deployer-mac-vbox.sh`
This script will source a variables file (bosh-cf-setup-vars-template.txt),
check to see if virtualbox is installed on a Mac, setup 'vboxnet0' if it doesn't
exist, setup 'natnetwork' if it doesn't exist, then download bosh and cf packages,
install bosh-lite and deploy cloud foundry. (aws creds are obviously not needed
in vars-template.txt file)

`bosh-cf-setup-vars-template.txt`
Template file for both deployment params.  Check the URLs for current versions and 
update accordingly. Make a copy of this template and populate the vars to be 
passed when calling `bosh-cf-deployer.sh`  or `bosh-lite-cf-deployer-mac-vbox.sh`.


###Pre-reqs for AWS Deployment.
* AWS account
* IAM key/secret and policy for bbl use (more details in `bosh-cf-deployer.sh`)
* Ubuntu 16 jumpbox/gateway instance deployed in AWS default VPC
* clone this repo on the jumpbox/gateway instance
* (optional) IAM key/secret and policy for s3 blobstore (more details in `operations/_aws-s3-blobstore-iam-policy.json`)
* (optional) S3 buckets for blobstore
* (optional) RDS databases for UAA, Diego, Cloud Controller (cc)

###Post Deployment:
* Update DNS or Route53 with *.cloud.example.com CNAME to stack-bbl-CFRouter-<>
* Upload wildcard cert or create one in AWS ACM
* Change ELB stack-bbl-CFRouter-<> to use ACM wildcard cert or real uploaded wildcard cert


###Credentials
* This setup will generate the `~/.bosh/config` creds file with the environment alias pulled in from `bosh-cf-setup-vars-template.txt`

