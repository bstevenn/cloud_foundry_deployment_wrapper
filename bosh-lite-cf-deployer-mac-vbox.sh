#!/bin/bash

#set -x 

# This script check for existance of VirtualBox and proceeds with Bosh and 
# CloudFoundry install if found.
# Find seed variables in "bosh-cf-setup-vars-template.txt" example

if [ ! "test -x /usr/local/bin/VirtualBox" ]; then
    echo "# It does not appear vitualbox is installed"
    echo ""
    echo "# Read This: https://github.com/cloudfoundry/bosh-deployment/blob/master/docs/bosh-lite-on-vbox.md"
    echo "# Try:"
    echo "#Fetch Oracle Virtual Box"
    echo "# More Info: https://www.virtualbox.org/wiki/Downloads"
    echo "cd $DEPLOY_DIR"
    echo "wget http://download.virtualbox.org/virtualbox/5.1.14/VirtualBox-5.1.14-112924-OSX.dmg"
    echo "sudo hdiutil attach $DEPLOY_DIR/VirtualBox-5.1.6-110634-OSX.dmg"
    echo "sudo installer -package /Volumes/VirtualBox/VirtualBox.pkg -target /"
    echo "sudo hdiutil detach /Volumes/VirtualBox"
    echo ""
    echo "#Get Oracle_VM_VirtualBox_Extension_Pack"
    echo "cd $DEPLOY_DIR"
    echo "wget http://download.virtualbox.org/virtualbox/5.1.14/Oracle_VM_VirtualBox_Extension_Pack-5.1.14-112924.vbox-extpack"
    echo "sudo vboxmanage extpack install $DEPLOY_DIR/Oracle_VM_VirtualBox_Extension_Pack-5.1.14-112924.vbox-extpack"
    exit 1
fi 

if [ ! $(/usr/bin/which jq) ]; then
    echo "'jq' not found. Please install 'jq - A lightweight and flexible command-line JSON processor'"
    echo "Try: port install jq"
    exit 1
fi

#Check to make sure the Host-Only network adapter is setup
if [ "vboxnet0" != "$(VBoxManage list hostonlyifs | grep "^Name:" | awk '{print$2}')" ] 
then 
    #echo "Read This: https://github.com/cppforlife/bosh-virtualbox-cpi-release/blob/master/docs/networks-host-only.md" 
    echo "# vboxnet0 not found"
    echo "# Creating Host-Only Network interface 'vboxnet0'"
    VBoxManage hostonlyif create
    VBoxManage hostonlyif ipconfig vboxnet0 --ip 192.168.50.1 --netmask 255.255.255.0
fi

#NAT Network set up allows multiple VMs to be on the same network and access network outside of the host
#https://github.com/cppforlife/bosh-virtualbox-cpi-release/blob/master/docs/networks-nat-network.md
NATNET="10.0.2.0/24"
if [ "NatNetwork" != "$(VBoxManage list natnetworks | grep ^NetworkName: | awk '{print$2}')" ] 
then 
    echo "# NatNetwork not found.  Setting up now with SUBNET $NATNET"
    echo "# More Info: #https://github.com/cppforlife/bosh-virtualbox-cpi-release/blob/master/docs/networks-nat-network.md"
    VBoxManage natnetwork add --netname NatNetwork --network $NATNET --dhcp on
    VBoxManage list natnetworks
elif [ "$NATNET" != "$(VBoxManage list natnetworks | grep "Network:" | awk '{print$2}')" ]
then
    echo "# NatNetwork found, but does not match expected value $NATNET"
    echo "# More Info: #https://github.com/cppforlife/bosh-virtualbox-cpi-release/blob/master/docs/networks-nat-network.md"
    exit 1
fi

#Source would be your populated file based on
#bosh-cf-setup-vars-template.txt or vars above ^^
if [ -z "$1" ]
then
    echo "# Please provide a populated <bosh-cf-setup-vars-template.txt> file"
    echo "# Example: $0 <bosh-cf-setup-vars-template.txt>"
    exit 1
else
    source $1
fi

if [ "$CF_DOMAIN" != "bosh-lite.com" ]
then
    echo "# Set CF_DOMAIN='bosh-lite.com' in $1"
    exit 1
fi

if [ ! -d "$DEPLOY_DIR" ]
then
    mkdir -p $DEPLOY_DIR
fi
 
#Install bosh-init for Mac
#http://bosh.io/docs/install-bosh-init.html
if [ ! -f $DEPLOY_DIR/bosh-init-${BOSH_INIT}-darwin-amd64 ]
then
    cd $DEPLOY_DIR
    wget https://s3.amazonaws.com/bosh-init-artifacts/bosh-init-${BOSH_INIT}-darwin-amd64
    chmod +x $DEPLOY_DIR/bosh-init-${BOSH_INIT}-darwin-amd64
    sudo cp $DEPLOY_DIR/bosh-init-${BOSH_INIT}-darwin-amd64 /usr/local/bin/bosh-init
    echo "# INSTALLED: BOSH-INIT"
else
    echo "# ALREADY INSTALLED: bosh-init-${BOSH_INIT}-darwin-amd64"
fi

#Install bosh-cli
#https://bosh.io/docs/cli-v2.html#install
if [ ! -f $DEPLOY_DIR/bosh-cli-${BOSH_CLI}-darwin-amd64 ]
then
    cd $DEPLOY_DIR
    wget https://s3.amazonaws.com/bosh-cli-artifacts/bosh-cli-${BOSH_CLI}-darwin-amd64
    chmod +x $DEPLOY_DIR/bosh-cli-${BOSH_CLI}-darwin-amd64
    sudo cp $DEPLOY_DIR/bosh-cli-${BOSH_CLI}-darwin-amd64 /usr/local/bin/bosh
    echo "# INSTALLED: Bosh-cli"
else
    echo "# ALREADY INSTALLED: bosh-cli-${BOSH_CLI}-darwin-amd64"
fi

#Clone bosh-deployment repo
if [ ! -d $DEPLOY_DIR/bosh-deployment ]
then
    cd $DEPLOY_DIR
    git clone https://github.com/cloudfoundry/bosh-deployment
    cd $DEPLOY_DIR/bosh-deployment
    git checkout $BOSH_DEPLOYMENT_BRANCH
    echo "# CLONED REPO: bosh-deployment"
else
    cd $DEPLOY_DIR/bosh-deployment
    git checkout $BOSH_DEPLOYMENT_BRANCH
    git pull
    echo "# UPDATED REPO: bosh-deployment"
    cd $DEPLOY_DIR
fi

#Clone cf-deployment repo
if [ ! -d $DEPLOY_DIR/cf-deployment ]
then
    cd $DEPLOY_DIR
    git clone https://github.com/cloudfoundry/cf-deployment.git
    cd $DEPLOY_DIR/cf-deployment
    git checkout $CF_DEPLOYMENT_BRANCH
    echo "# CLONED REPO: cf-deployment"
else
    cd $DEPLOY_DIR/cf-deployment
    git checkout $CF_DEPLOYMENT_BRANCH
    git pull
    echo "# UPDATED REPO: cf-deployment"
    cd $DEPLOY_DIR
fi


#Deploy/update Bosh instance in virtualbox
if [ ! -f $DEPLOY_DIR/state.json ] 
then
    BOSH_CMD="create-env"
    echo "# Setting up bosh: bosh $BOSH_CMD"
    bosh $BOSH_CMD $DEPLOY_DIR/bosh-deployment/bosh.yml \
      --state $DEPLOY_DIR/state.json \
      -o $DEPLOY_DIR/bosh-deployment/virtualbox/cpi.yml \
      -o $DEPLOY_DIR/bosh-deployment/virtualbox/outbound-network.yml \
      -o $DEPLOY_DIR/bosh-deployment/bosh-lite.yml \
      -o $DEPLOY_DIR/bosh-deployment/jumpbox-user.yml \
      # below was causing issues but worked last run
      -o $DEPLOY_DIR/bosh-deployment/bosh-lite-runc.yml \
      --vars-store $DEPLOY_DIR/creds.yml \
      -v director_name="Bosh Lite Director" \
      -v internal_ip=192.168.50.6\
      -v internal_gw=192.168.50.1 \
      -v internal_cidr=192.168.50.0/24 \
      -v network_name=vboxnet0 \
      -v outbound_network_name=NatNetwork
    # remove this for now
    # https://cloudfoundry.slack.com/archives/release-integration/p1485212420001756
    # -o $DEPLOY_DIR/bosh-deployment/bosh-lite-runc.yml \
else 
    BOSH_CMD="cloud-check"
    echo "# Bosh $DEPLOY_DIR/state.json found.  Verifying deployment: bosh $BOSH_CMD"
    bosh \
      -e $BOSH_ALIAS \
      -d cf \
      $BOSH_CMD 
fi

#Setup routes to access VMs on hosted in 10.244/24 on warden containers
WARDEN_NET='10.244.0.0/24' #This shouldn't change since bosh-lite.com has DNS setup for it
BASE_NET=$(echo $WARDEN_NET | cut -d"/" -f1)
if [ "$BASE_NET" != "$(route get $WARDEN_NET | grep "route to:" | awk '{print$3}')" ]
then
    echo "# Adding local route to access $WARDEN_NET behind bosh director"
    sudo route add $WARDEN_NET 192.168.50.6
fi

#Setup Alias
echo "# Setting up bosh alias to '$BOSH_ALIAS' using 'bosh alias-env'"
bosh alias-env $BOSH_ALIAS -e 192.168.50.6 --ca-cert <(bosh int $DEPLOY_DIR/creds.yml --path /director_ssl/ca)
echo ""

#Login to bosh and create ~/.bosh/config file
echo "# Creating ~/.bosh/config file using 'bosh log-in' cmd"
bosh login -e 192.168.50.6 --client=admin --client-secret=$(bosh int $DEPLOY_DIR/creds.yml --path /admin_password)

#Upload Stemcell
STEMCELL="bosh-warden-boshlite-ubuntu-trusty-go_agent"
if [ "$STEMCELL" != $(bosh -e vbox stemcells --json | jq .Tables[0].Rows[0][0] | cut -d '"' -f 2) ]
then
    bosh -e $BOSH_ALIAS \
    upload-stemcell https://bosh.io/d/stemcells/bosh-warden-boshlite-ubuntu-trusty-go_agent \
    --non-interactive
fi

#Update Cloud Config
#https://github.com/cloudfoundry/cf-deployment#deploying-to-bosh-lite
bosh -e $BOSH_ALIAS \
    update-cloud-config $DEPLOY_DIR/cf-deployment/bosh-lite/cloud-config.yml \
    --non-interactive

#Deploy Cloud Foundry
bosh -e $BOSH_ALIAS \
    -d cf \
    deploy $DEPLOY_DIR/cf-deployment/cf-deployment.yml \
    -o $DEPLOY_DIR/cf-deployment/operations/bosh-lite.yml \
    --var system_domain=$CF_DOMAIN \
    --vars-store $DEPLOY_DIR/deployment-vars.yml \
    --non-interactive 

#Helper output for how to ssh to bosh director VM
#Need to parse creds.yml file
if [ ! -f $DEPLOY_DIR/jumpbox.key ]
then
    echo "# How to ssh into bosh director"
    #Create directory private key from creds file and modify permissions
    bosh int $DEPLOY_DIR/creds.yml --path /jumpbox_ssh/private_key > $DEPLOY_DIR/jumpbox.key
    chmod 600 $DEPLOY_DIR/jumpbox.key
    echo "# ssh -l jumpbox -i $DEPLOY_DIR/jumpbox.key 192.168.50.6"
else
    echo "# How to ssh into bosh director"
    echo "ssh -l jumpbox -i $DEPLOY_DIR/jumpbox.key 192.168.50.6"
fi

#pivotal.io already has wildcard DNS for bosh-lite.com mapped to private space
echo ""
echo "# Pivotal.io already has wildcard DNS for bosh-lite.com mapped to private space"
echo "# Make sure your local nameserver can resolve bosh-lite.com. Nameserver"
echo "# 8.8.8.8 is known to work.  The domain <*>.bosh-lite.com should resolve to 10.244.0.34"
echo "# Try: dig bosh-lite.com ANY"

#Login to Cloudfoundry:
cd $DEPLOY_DIR
CF_PASSWORD=$(cat $DEPLOY_DIR/deployment-vars.yml | grep uaa_scim_users_admin_password | cut -d" " -f 2)
echo ""
echo "# Login to cloud foundry with the following command.  If this is the first time, create an ORG and SPACE"
echo "cf login -a https://api.$CF_DOMAIN -u admin -p $CF_PASSWORD --skip-ssl-validation"
