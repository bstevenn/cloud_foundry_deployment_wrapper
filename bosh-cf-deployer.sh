#!/bin/bash

#set -x

#Pre-Reqs
#1) Ubuntu 16 instance in default AWS VPC
#2) AWS IAM Key/Secret with policy:
#https://github.com/cloudfoundry/bosh-bootloader
    #{
    #    "Version": "2012-10-17",
    #    "Statement": [
    #        {
    #            "Effect": "Allow",
    #            "Action": [
    #                "ec2:*",
    #                "cloudformation:*",
    #                "elasticloadbalancing:*",
    #                "iam:*"
    #            ],
    #            "Resource": [
    #                "*"
    #            ]
    #        }
    #}
#Set tab var used to help format screen output
TAB="$(printf '\t')"

# Check OS version is Ubuntu 16.04 or greater
OS=($(lsb_release -isr))
if [[ ("${OS[0]}" != "Ubuntu") || ("${OS[1]}" < "16.04") ]]
then
  echo "# This should be run on Ubuntu 16.04 or later. Detected ${OS[@]}"
  exit 1
fi

#"Source" would be your populated file based on
#a copy of bosh-cf-setup-vars-template.txt
if [ -z "$1" ]
then
    echo "# Please provide a populated <bosh-cf-setup-vars-template.txt> file"
    echo "# Example: $0 <bosh-cf-setup-vars-template.txt>"
    exit 1
else
    source $1
fi

if [ ! -d "$DEPLOY_DIR" ]
then
    mkdir -p $DEPLOY_DIR
fi

#Define current directory
LAUNCH_DIR=$(dirname $(readlink -f $0))
echo $LAUNCH_DIR

#Seed Cloud Foundry Cli Apt Repo
if [ ! -f /etc/apt/sources.list.d/cloudfoundry-cli.list ]
then
    cd $DEPLOY_DIR
    wget -q -O - https://packages.cloudfoundry.org/debian/cli.cloudfoundry.org.key | sudo apt-key add -
    sudo echo "deb http://packages.cloudfoundry.org/debian stable main" | sudo tee /etc/apt/sources.list.d/cloudfoundry-cli.list
fi

#Update Ubuntu and install required packages
echo "# Updating OS and installing required packages"
#Using -qq despite man page warnings.  Assuming this is just a jumbox
sudo apt-get update  -qq
#sudo apt-get upgrade -y -q
sudo apt-get install -y -qq cf-cli build-essential golang-go zlibc zlib1g-dev ruby \
    ruby-dev openssl libxslt-dev libxml2-dev libssl-dev libreadline6 \
    libreadline6-dev libyaml-dev libsqlite3-dev sqlite3
sudo apt autoremove -y -qq

#Install Terraform
#https://releases.hashicorp.com/terraform/
if [ ! -f $DEPLOY_DIR/terraform_${TERRAFORM}_linux_amd64.zip ]
then
    cd $DEPLOY_DIR
    wget https://releases.hashicorp.com/terraform/${TERRAFORM}/terraform_${TERRAFORM}_linux_amd64.zip
    unzip $DEPLOY_DIR/terraform_${TERRAFORM}_linux_amd64.zip
    sudo cp $DEPLOY_DIR/terraform /usr/bin/terraform
    echo "# INSTALLED: TERRAFORM"
else
    echo "# ALREADY INSTALLED: terraform_${TERRAFORM}_linux_amd64.zip"
fi

#Install bosh-init
if [ ! -f $DEPLOY_DIR/bosh-init-${BOSH_INIT}-linux-amd64 ]
then
    cd $DEPLOY_DIR
    wget https://s3.amazonaws.com/bosh-init-artifacts/bosh-init-${BOSH_INIT}-linux-amd64
    chmod +x $DEPLOY_DIR/bosh-init-${BOSH_INIT}-linux-amd64
    sudo cp $DEPLOY_DIR/bosh-init-${BOSH_INIT}-linux-amd64 /usr/bin/bosh-init
    echo "# INSTALLED: BOSH-INIT"
else
    echo "# ALREADY INSTALLED: bosh-init-${BOSH_INIT}-linux-amd64"
fi

#Install bosh-bootloader
if [[ ! -f $DEPLOY_DIR/bbl-v${BBL}_linux_x86-64 || $(bbl version | awk '{print$2}') != $BBL ]]
then
    cd $DEPLOY_DIR
    wget https://github.com/cloudfoundry/bosh-bootloader/releases/download/v$BBL/bbl-v${BBL}_linux_x86-64
    chmod +x $DEPLOY_DIR/bbl-v${BBL}_linux_x86-64
    sudo cp $DEPLOY_DIR/bbl-v${BBL}_linux_x86-64 /usr/bin/bbl
    echo "# INSTALLED: Bosh-bootloader"
else
    echo "# ALREADY INSTALLED: bbl-${BBL}_linux_x86-64"
fi

#Install bosh-cli
if [ ! -f $DEPLOY_DIR/bosh-cli-${BOSH_CLI}-linux-amd64 ]
then
    cd $DEPLOY_DIR
    wget https://s3.amazonaws.com/bosh-cli-artifacts/bosh-cli-${BOSH_CLI}-linux-amd64
    chmod +x $DEPLOY_DIR/bosh-cli-${BOSH_CLI}-linux-amd64
    sudo cp $DEPLOY_DIR/bosh-cli-${BOSH_CLI}-linux-amd64 /usr/bin/bosh
    echo "# INSTALLED: Bosh-cli"
else
    echo "# ALREADY INSTALLED: bosh-cli-${BOSH_CLI}-linux-amd64"
fi

#Clone bosh-deployment repo
if [ ! -d $DEPLOY_DIR/bosh-deployment ]
then
    cd $DEPLOY_DIR
    git clone https://github.com/cloudfoundry/bosh-deployment
    git checkout $BOSH_DEPLOYMENT_BRANCH
    echo "# CLONED REPO: bosh-deployment"
else
    cd $DEPLOY_DIR/bosh-deployment
    git checkout $BOSH_DEPLOYMENT_BRANCH
    git pull
    echo "# UPDATED REPO: bosh-deployment"
    cd $DEPLOY_DIR
fi

#Clone cf-deployment repo
if [ ! -d $DEPLOY_DIR/cf-deployment ]
then
    cd $DEPLOY_DIR
    git clone https://github.com/cloudfoundry/cf-deployment.git
    git checkout $CF_DEPLOYMENT_BRANCH
    echo "# CLONED REPO: cf-deployment"
else
    cd $DEPLOY_DIR/cf-deployment
    git checkout $CF_DEPLOYMENT_BRANCH
    git pull
    echo "# UPDATED REPO: cf-deployment"
    cd $DEPLOY_DIR
fi

#Create self signed cert since they currently don't support AWS Certificate Manager
if [ ! -f $DEPLOY_DIR/$CF_DOMAIN-ss2.crt ]
then
    openssl genrsa -des3 -passout pass:x -out $DEPLOY_DIR/$CF_DOMAIN-ss2.pass.key 2048
    openssl rsa -passin pass:x -in $DEPLOY_DIR/$CF_DOMAIN-ss2.pass.key -out $DEPLOY_DIR/$CF_DOMAIN-ss2.key
    openssl req -subj "/C=${COUNTRY}/ST=${STATE}/L=${CITY}/O=${ORG}/OU=${OU}/CN=*.${CF_DOMAIN}/emailAddress=admin@${CF_DOMAIN}" \
        -new \
        -key $DEPLOY_DIR/$CF_DOMAIN-ss2.key \
        -out $DEPLOY_DIR/$CF_DOMAIN-ss2.csr
    openssl x509 -req -sha256 -days 7300 -in $DEPLOY_DIR/$CF_DOMAIN-ss2.csr -signkey $DEPLOY_DIR/$CF_DOMAIN-ss2.key -out $DEPLOY_DIR/$CF_DOMAIN-ss2.crt
    echo "# CERT CHAIN CREATED"
    echo ""
else
    echo "# CERT FOUND"
    echo ""
fi

#This will create/update the VPC, Subnets, sec groups, routers, Bosh/0 (director), NAT, etc, using a cloudformation manifest/template
if [ ! -f $DEPLOY_DIR/bbl-state.json ]; then
    cd $DEPLOY_DIR
    bbl up --aws-access-key-id $AWS_KEY --aws-secret-access-key $AWS_SECRET --aws-region $AWS_REGION --iaas aws
elif [ "$UPDATE_DIRECTOR" = true ]; then
    bbl --state-dir $DEPLOY_DIR up --aws-access-key-id $AWS_KEY --aws-secret-access-key $AWS_SECRET --aws-region $AWS_REGION --iaas aws
else
   echo "# BOSH DEPLOYMENT EXISTS: bbl-state.json file found"
   echo "# Run the following command to manually update:"
   cat << EOF 
${TAB} bbl \\
${TAB} --state-dir $DEPLOY_DIR \\
${TAB} up \\
${TAB} --aws-access-key-id $AWS_KEY \\
${TAB} --aws-secret-access-key $AWS_SECRET \\
${TAB} --aws-region $AWS_REGION \\
${TAB} --iaas aws
EOF
echo ""
fi

#Create ELB with Cert
cd $DEPLOY_DIR
bbl create-lbs --type cf --cert $DEPLOY_DIR/${CF_DOMAIN}-ss2.crt --key $DEPLOY_DIR/${CF_DOMAIN}-ss2.key --skip-if-exists

#Check deploy directory and set variables from bbl created creds and environment
if [ -d $DEPLOY_DIR ]
then
    BOSH_CLIENT=$(bbl director-username)
    BOSH_CLIENT_SECRET=$(bbl director-password)
    BOSH_ENVIRONMENT=$(bbl director-address)
    bbl director-ca-cert > $DEPLOY_DIR/director.ca
    BOSH_CA_CERT="$DEPLOY_DIR/director.ca"
    BOSH_DEPLOYMENT="$DEPLOYMENT_ALIAS"
fi

#Upload Stemcells (this needs to match the stemcell version in my-deployments)
bosh -e $BOSH_ENVIRONMENT \
    --client=$BOSH_CLIENT \
    --client-secret=$BOSH_CLIENT_SECRET \
    --ca-cert=$BOSH_CA_CERT \
    --deployment=$BOSH_DEPLOYMENT \
     upload-stemcell https://bosh.io/d/stemcells/bosh-aws-xen-hvm-ubuntu-trusty-go_agent

# Copy any specified Operations file from the source repo into the Deployment Directory
# First create the deployment specific operations file directory
DEPLOY_OPS_DIR=$BOSH_ALIAS-specified-cf-operations
if [ ! -d $DEPLOY_DIR/$DEPLOY_OPS_DIR ]
then
    mkdir -p $DEPLOY_DIR/$DEPLOY_OPS_DIR
fi
# Move all Operations files that are defined in the vars file to the 
# Deploy directory.  
for f in ${OPS_FILES[@]}
do
  echo "# Staging the following Operations file: $f"
  cp $LAUNCH_DIR/operations/$f $DEPLOY_DIR/$DEPLOY_OPS_DIR
  OPS_FILE_CMD+=("-o $DEPLOY_DIR/$DEPLOY_OPS_DIR/$f")
done
echo ""


#Function to cleanly echo output of the operations file flags to the terminal
function OPS_CMD_BUILDER {
    for ((i=0; i < ${#OPS_FILE_CMD[@]}; i++)); do
        echo "${TAB} ${OPS_FILE_CMD[$i]} \\"
    done
}
#Echo Command to terminal so user can see what command is being run to deploy
#Cloud foundry
echo "# Deploying Cloud Foundry with the following command:"
echo ""
cat <<EOF
${TAB} bosh -e $BOSH_ENVIRONMENT \\
${TAB} --client=$BOSH_CLIENT \\
${TAB} --client-secret=$BOSH_CLIENT_SECRET \\
${TAB} --ca-cert=$BOSH_CA_CERT \\
${TAB} --deployment=$BOSH_DEPLOYMENT \\
${TAB} --non-interactive \\
${TAB} deploy $DEPLOY_DIR/cf-deployment/cf-deployment.yml \\
${TAB} -o $DEPLOY_DIR/cf-deployment/operations/change-logging-port-for-aws-elb.yml \\
${TAB} -o $DEPLOY_DIR/cf-deployment/operations/disable-router-tls-termination.yml \\
$(OPS_CMD_BUILDER)
${TAB} --var system_domain=$CF_DOMAIN \\
${TAB} --vars-store $DEPLOY_DIR/deployment-vars.yml \\
${TAB} $TEST
EOF
if [ "$UPDATE_CF" = true ]; then
    NO_PROMPT='--non-interactive'
fi

bosh -e $BOSH_ENVIRONMENT \
    --client=$BOSH_CLIENT \
    --client-secret=$BOSH_CLIENT_SECRET \
    --ca-cert=$BOSH_CA_CERT \
    --deployment=$BOSH_DEPLOYMENT \
    deploy $DEPLOY_DIR/cf-deployment/cf-deployment.yml \
    $NO_PROMPT \
    -o $DEPLOY_DIR/cf-deployment/operations/change-logging-port-for-aws-elb.yml \
    -o $DEPLOY_DIR/cf-deployment/operations/disable-router-tls-termination.yml \
    ${OPS_FILE_CMD[@]} \
    --var system_domain=$CF_DOMAIN \
    --vars-store $DEPLOY_DIR/deployment-vars.yml \
    $TEST

echo "############################################################################"
echo "##################### GETTING STARTED NOTES  ###############################"
echo "############################################################################"
echo ""
echo "# Creating ~/.bosh/config file using 'bosh log-in' cmd"
bosh -e $BOSH_ENVIRONMENT \
    --client=$BOSH_CLIENT \
    --client-secret=$BOSH_CLIENT_SECRET \
    --ca-cert=$BOSH_CA_CERT \
    --deployment=$BOSH_DEPLOYMENT \
    --non-interactive \
    log-in

echo "# Setting up bosh alias to '$BOSH_ALIAS' using 'bosh alias-env'"
bosh -e $BOSH_ENVIRONMENT \
    --client=$BOSH_CLIENT \
    --client-secret=$BOSH_CLIENT_SECRET \
    --ca-cert=$BOSH_CA_CERT \
    --deployment=$BOSH_DEPLOYMENT \
    --non-interactive \
    alias-env $BOSH_ALIAS

echo "# Use bosh commands with 'bosh -e $BOSH_ALIAS -d $BOSH_DEPLOYMENT <command>' syntax"
echo -e '\t' "Example: 'bosh -e $BOSH_ALIAS -d $BOSH_DEPLOYMENT vms'"
echo ""


# How to check test the manifest generation (interpolate)
echo "# How to check/test the manifest generation (interpolate):"
echo ""
cat <<EOF
${TAB} bosh -e $BOSH_ALIAS \\
${TAB} --client=$BOSH_CLIENT \\
${TAB} --client-secret=$BOSH_CLIENT_SECRET \\
${TAB} --ca-cert=$BOSH_CA_CERT \\
${TAB} --deployment=$BOSH_DEPLOYMENT \\
${TAB} interpolate $DEPLOY_DIR/cf-deployment/cf-deployment.yml \\
${TAB} -o $DEPLOY_DIR/cf-deployment/operations/change-logging-port-for-aws-elb.yml \\
${TAB} -o $DEPLOY_DIR/cf-deployment/operations/disable-router-tls-termination.yml \\
$(OPS_CMD_BUILDER) 
${TAB} --var system_domain=$CF_DOMAIN \\
${TAB} --vars-store $DEPLOY_DIR/deployment-vars.yml
EOF
echo ""

#Login to Cloudfoundry:
cd $DEPLOY_DIR
MAIN_ELB=($(bbl lbs | grep "Router"))
echo "# Make sure DNS (Route53) *.$CF_DOMAIN has a CNAME to"
echo "# ELB ${MAIN_ELB[-1]}"
echo "# (Optional) Update real wildcard cert on"
echo "# ELB ${MAIN_ELB[-1]}"
CF_PASSWORD=$(cat $DEPLOY_DIR/deployment-vars.yml | grep uaa_scim_users_admin_password | cut -d" " -f 2)
echo ""
echo "# Login to cloud foundry with the following command."
echo "# If this is the first time, create an ORG and SPACE"
echo "# Admin Login:"
echo -e '\t' "cf login -a https://api.$CF_DOMAIN -u admin -p $CF_PASSWORD --skip-ssl-validation"
echo ""

#How to used bosh ssh.  You need to get the private key from bbl (bosh bootloader)
BOSH_KEY=$DEPLOY_DIR/bosh-ssh.key
if [ ! -f $BOSH_KEY ]
then
    bbl ssh-key > $BOSH_KEY
    chmod 600 $BOSH_KEY
fi
echo "# How to SSH into CF instances for troubleshooting:"
echo -e '\t' "bosh -e apac2 -d $BOSH_DEPLOYMENT ssh --gw-private-key=$BOSH_KEY <uaa/<instance id>"
echo ""

#Create a backup of the manifest
TIMESTAMP=$(date +%Y%m%d-%H%M%S)
MAN_BACKUPS=$DEPLOY_DIR/manifest_backups
if [ ! -d $MAN_BACKUPS ]
then
    mkdir -p $MAN_BACKUPS
fi
echo "# Creating a backup of the manifest.  Saving to:"
echo -e '\t' "$MAN_BACKUPS/manifest_backup_$TIMESTAMP.yml"
bosh -e $BOSH_ALIAS -d $BOSH_DEPLOYMENT manifest > $MAN_BACKUPS/manifest_backup_$TIMESTAMP.yml
echo ""

echo "# Creating a backup of the var-store.  Saving to:"
echo -e '\t' "$MAN_BACKUPS/deployment-vars_$TIMESTAMP.yml"
cp $DEPLOY_DIR/deployment-vars.yml $MAN_BACKUPS/deployment-vars_$TIMESTAMP.yml
echo ""
echo "############################################################################"
echo "############################################################################"
